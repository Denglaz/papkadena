﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vectred
{
    class ObjectField
    {
        //Image trpt = Image.FromFile(@"Images\transparent.png");
        Point c1, c2, c3, c4, p1, p2;
        Form1 mForm;
        Panel p;
        Graphics g;
        bool right;
        string ins;

        public ObjectField(Point p1, Point p2, Form1 mForm)
        {
            this.mForm = mForm;
            this.p1 = p1;
            this.p2 = p2;
            if(p1.X < p2.X && p1.Y < p2.Y)
            {
                c1 = p1;
                c2 = new Point(p2.X, p1.Y);
                c3 = p2;
                c4 = new Point(p1.X, p2.Y);
                right = true;
            }
            else if(p2.X < p1.X && p2.Y < p1.Y)
            {
                c1 = p2;
                c2 = new Point(p1.X, p2.Y);
                c3 = p1;
                c4 = new Point(p2.X, p1.Y);
                right = true;
            }
            else if(p1.X > p2.X && p2.Y > p1.Y)
            {
                c1 = new Point(p2.X, p1.Y);
                c2 = p1;
                c3 = new Point(p1.X, p2.Y);
                c4 = p2;
                right = false;
            }
            else if(p1.X < p2.X && p1.Y > p2.Y)
            {
                c1 = new Point(p1.X, p2.Y);
                c2 = p2;
                c3 = new Point(p2.X, p1.Y);
                c4 = p1;
                right = false;
            }
            p = new Panel();
            p.Parent = mForm.canvas;
            p.Location = c1;
            p.Size = new Size(c3.X - c1.X, c3.Y - c1.Y);
            g = p.CreateGraphics();
            p.BringToFront();
        }

        public Panel panel
        {
            get { return p; }
        }

        public void Dispose()
        {
            p.Dispose();
            Dispose();
        }

        public void DrawLine(Pen pen)
        {
            if (right)
                g.DrawLine(pen, 0, 0, p.Width, p.Height);
            else
                g.DrawLine(pen, p.Width, 0, 0, p.Height);
            ins = "line";
        }

        public void DrawRectangle(Pen pen)
        {
            g.DrawRectangle(pen, new Rectangle(0, 0, p.Width - 1, p.Height - 1));
            ins = "rectangle";
        }

        public void DrawEllipse(Pen pen)
        {
            g.DrawEllipse(pen, new Rectangle(0, 0, p.Width - 1, p.Height - 1));
            ins = "ellipse";
        }

        public void Print(Graphics g, Pen pen)
        {
            
        }
    }
}
