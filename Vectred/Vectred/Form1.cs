﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vectred
{
    public partial class Form1 : Form
    {
        Point p1, p2;
        List<ObjectField> objects = new List<ObjectField>();
        Instruments ins;
        Graphics g;
        Pen defaultPen = new Pen(Color.Black);
        GraphicsState st;
        Bitmap bm;

        public Form1()
        {
            InitializeComponent();
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            KeyPreview = true;
            g = canvas.CreateGraphics();
            bm = new Bitmap(canvas.Width, canvas.Height);
            
        }

        enum Instruments
        {
            Line = 1,
            Rectangle,
            Ellipse
        };

        private void canvas_MouseUp(object sender, MouseEventArgs e)
        {
            objects.Add(new ObjectField(p1, e.Location, this));
            canvas.Controls.Add(objects.Last().panel);
            if (ins == Instruments.Line)
                objects.Last().DrawLine(defaultPen);
            else if (ins == Instruments.Rectangle)
                objects.Last().DrawRectangle(defaultPen);
            else if (ins == Instruments.Ellipse)
                objects.Last().DrawEllipse(defaultPen);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (objects.Count > 0)
            {
                objects.Last().Print(g, defaultPen);
                objects.Remove(objects.Last());
            }
        }

        private void LineButton_Click(object sender, EventArgs e)
        {
            ins = Instruments.Line;
        }

        private void RectangleButton_Click(object sender, EventArgs e)
        {
            ins = Instruments.Rectangle;
        }

        private void EllipseButton_Click(object sender, EventArgs e)
        {
            ins = Instruments.Ellipse;
        }

        public void canvas_MouseDown(object sender, MouseEventArgs e)
        {
             p1 = e.Location;
        }
    }
}
