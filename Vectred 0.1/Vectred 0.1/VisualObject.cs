﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vectred_0._1
{
    public abstract class VisualObject
    {
        Color pc;
        Color bc;
        Point p1, p2;
        Point c1, c2, c3, c4;
        Region tl, tm, tr, bl, bm, br, lm, rm;
        public string vect;
        bool filled;
        bool selected = false;

        public virtual Color PenColor
        {
            get { return pc; }
            set { pc = value; }
        }

        public virtual Color BrushColor
        {
            get { return bc; }
            set { bc = value; }
        }

        public virtual Point StartPoint
        {
            get { return p1; }
            set { p1 = value; }
        }

        public virtual Point EndPoint
        {
            get { return p2; }
            set { p2 = value; }
        }

        #region координаты

        public virtual Point Coord1
        {
            get { return c1; }
            set { c1 = value; }
        }

        public virtual Point Coord2
        {
            get { return c2; }
            set { c2 = value; }
        }

        public virtual Point Coord3
        {
            get { return c3; }
            set { c3 = value; }
        }

        public virtual Point Coord4
        {
            get { return c4; }
            set { c4 = value; }
        }

        #endregion

        public virtual Region Region
        {
            get { return new Region(GetRectangle()); }
        }

        #region регионы редактирования

        public virtual Region TopLeft
        {
            get { return tl; }
        }

        public virtual Region TopMiddle
        {
            get { return tm; }
        }

        public virtual Region TopRight
        {
            get { return tr; }
        }

        public virtual Region BottomLeft
        {
            get { return bl; }
        }

        public virtual Region BottomMiddle
        {
            get { return bm; }
        }

        public virtual Region BottomRight
        {
            get { return br; }
        }

        public virtual Region LeftMiddle
        {
            get { return lm; }
        }

        public virtual Region RightMiddle
        {
            get { return rm; }
        }

        #endregion

        public virtual bool Filled
        {
            get { return filled; }
            set { filled = value; }
        }

        public virtual bool Selected
        {
            get { return selected; }
            set { selected = value; }
        }

        public abstract void Draw(Graphics g);

        public abstract bool HitTest(Point p);

        public virtual void SetCoords()
        {
            if (p1.X < p2.X && p1.Y < p2.Y)
            {
                c1 = p1;
                c2 = new Point(p2.X, p1.Y);
                c3 = p2;
                c4 = new Point(p1.X, p2.Y);
                vect = "\\";
            }
            else if (p2.X < p1.X && p2.Y < p1.Y)
            {
                c1 = p2;
                c2 = new Point(p1.X, p2.Y);
                c3 = p1;
                c4 = new Point(p2.X, p1.Y);
                vect = "\\";
            }
            else if (p1.X > p2.X && p2.Y > p1.Y)
            {
                c1 = new Point(p2.X, p1.Y);
                c2 = p1;
                c3 = new Point(p1.X, p2.Y);
                c4 = p2;
                vect = "/";
            }
            else if (p1.X < p2.X && p1.Y > p2.Y)
            {
                c1 = new Point(p1.X, p2.Y);
                c2 = p2;
                c3 = new Point(p2.X, p1.Y);
                c4 = p1;
                vect = "/";
            }
        }

        public virtual Rectangle GetRectangle()
        {
            return new Rectangle(c1, GetSize());
        }

        public virtual Size GetSize()
        {
            return new Size(c2.X - c1.X, c4.Y - c1.Y);
        }

        public virtual void DrawOverlay(Graphics g)
        {
            if (selected)
            {
                Rectangle rctg = new Rectangle(GetRectangle().Left - 8, GetRectangle().Top - 8, 8, 8);
                g.FillRectangle(Brushes.Black, rctg);
                tl = new Region(rctg);
                rctg = new Rectangle(GetRectangle().Right, GetRectangle().Top - 8, 8, 8);
                g.FillRectangle(Brushes.Black, rctg);
                tr = new Region(rctg);
                rctg = new Rectangle(GetRectangle().Left - 8, GetRectangle().Bottom, 8, 8);
                g.FillRectangle(Brushes.Black, rctg);
                bl = new Region(rctg);
                rctg = new Rectangle(GetRectangle().Right, GetRectangle().Bottom, 8, 8);
                g.FillRectangle(Brushes.Black, rctg);
                br = new Region(rctg);

                rctg = new Rectangle(GetRectangle().Left + GetRectangle().Width / 2 - 4, GetRectangle().Top - 8, 8, 8);
                g.FillRectangle(Brushes.Black, rctg);
                tm = new Region(rctg);
                rctg = new Rectangle(GetRectangle().Left - 8, GetRectangle().Top + GetRectangle().Height / 2 - 4, 8, 8);
                g.FillRectangle(Brushes.Black, rctg);
                lm = new Region(rctg);
                rctg = new Rectangle(GetRectangle().Left + GetRectangle().Width / 2 - 4, GetRectangle().Bottom, 8, 8);
                g.FillRectangle(Brushes.Black, rctg);
                bm = new Region(rctg);
                rctg = new Rectangle(GetRectangle().Right, GetRectangle().Top + GetRectangle().Height / 2 - 4, 8, 8);
                g.FillRectangle(Brushes.Black, rctg);
                rm = new Region(rctg);

                g.DrawRectangle(Pens.CadetBlue, GetRectangle());
            }
        }
    }

    class VisualLine : VisualObject
    {
        public VisualLine(Pen pen, Point startPoint, Point endPoint)
        {
            PenColor = pen.Color;
            StartPoint = startPoint;
            EndPoint = endPoint;
            SetCoords();
            Filled = false;
        }

        public override void Draw(Graphics g)
        {
            if (vect == "\\")
                g.DrawLine(new Pen(PenColor), Coord1, Coord3);
            else
                g.DrawLine(new Pen(PenColor), Coord2, Coord4);
        }

        public override bool HitTest(Point p)
        {
            return Region.IsVisible(p);
        }
    }

    class VisualRectangle : VisualObject
    {
        public VisualRectangle(Pen pen, Point startPoint, Point endPoint)
        {
            PenColor = pen.Color;
            StartPoint = startPoint;
            EndPoint = endPoint;
            SetCoords();
            Filled = false;
        }
        public VisualRectangle(Pen pen, SolidBrush brush, Point startPoint, Point endPoint)
        {
            PenColor = pen.Color;
            BrushColor = brush.Color;
            StartPoint = startPoint;
            EndPoint = endPoint;
            SetCoords();
            Filled = true;
        }

        public override void Draw(Graphics g)
        {
            g.DrawRectangle(new Pen(PenColor), GetRectangle());
            if (Filled)
                g.FillRectangle(new SolidBrush(BrushColor), GetRectangle());
        }

        public override bool HitTest(Point p)
        {
            return Region.IsVisible(p);
        }
    }

    class VisualEllipse : VisualObject
    {
        public VisualEllipse(Pen pen, Point startPoint, Point endPoint)
        {
            PenColor = pen.Color;
            StartPoint = startPoint;
            EndPoint = endPoint;
            SetCoords();
            Filled = false;
        }
        public VisualEllipse(Pen pen, SolidBrush brush, Point startPoint, Point endPoint)
        {
            PenColor = pen.Color;
            BrushColor = brush.Color;
            StartPoint = startPoint;
            EndPoint = endPoint;
            SetCoords();
            Filled = true;
        }

        public override void Draw(Graphics g)
        {
            g.DrawEllipse(new Pen(PenColor), GetRectangle());
            if (Filled)
                g.FillEllipse(new SolidBrush(BrushColor), GetRectangle());
        }

        public override bool HitTest(Point p)
        {
            return Region.IsVisible(p);
        }
    }
}
