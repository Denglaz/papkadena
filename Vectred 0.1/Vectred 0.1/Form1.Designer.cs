﻿namespace Vectred_0._1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.canvas = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripNewFile = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripCancel = new System.Windows.Forms.ToolStripMenuItem();
            this.CoordsLabel = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.contur = new System.Windows.Forms.Panel();
            this.filling = new System.Windows.Forms.Panel();
            this.fillcheck = new System.Windows.Forms.CheckBox();
            this.InsLine = new System.Windows.Forms.Panel();
            this.InsCursor = new System.Windows.Forms.Panel();
            this.InsEllipse = new System.Windows.Forms.Panel();
            this.InsRectangle = new System.Windows.Forms.Panel();
            this.отменитьВыделениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.canvas)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // canvas
            // 
            this.canvas.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.canvas.BackColor = System.Drawing.Color.White;
            this.canvas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.canvas.Cursor = System.Windows.Forms.Cursors.Default;
            this.canvas.Location = new System.Drawing.Point(84, 6);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(600, 400);
            this.canvas.TabIndex = 0;
            this.canvas.TabStop = false;
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            this.canvas.MouseClick += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseClick);
            this.canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseDown);
            this.canvas.MouseEnter += new System.EventHandler(this.canvas_MouseEnter);
            this.canvas.MouseLeave += new System.EventHandler(this.canvas_MouseLeave);
            this.canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseMove);
            this.canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseUp);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(691, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenFile,
            this.SaveAs,
            this.отменитьВыделениеToolStripMenuItem,
            this.ToolStripNewFile,
            this.ToolStripCancel});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // OpenFile
            // 
            this.OpenFile.Name = "OpenFile";
            this.OpenFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.OpenFile.Size = new System.Drawing.Size(202, 22);
            this.OpenFile.Text = "Открыть";
            this.OpenFile.Click += new System.EventHandler(this.OpenFile_Click);
            // 
            // SaveAs
            // 
            this.SaveAs.Name = "SaveAs";
            this.SaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.SaveAs.Size = new System.Drawing.Size(202, 22);
            this.SaveAs.Text = "Сохранить как...";
            this.SaveAs.Click += new System.EventHandler(this.SaveAs_Click);
            // 
            // ToolStripNewFile
            // 
            this.ToolStripNewFile.Name = "ToolStripNewFile";
            this.ToolStripNewFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.ToolStripNewFile.Size = new System.Drawing.Size(202, 22);
            this.ToolStripNewFile.Text = "Очистить";
            this.ToolStripNewFile.Click += new System.EventHandler(this.TSNewFile);
            // 
            // ToolStripCancel
            // 
            this.ToolStripCancel.Name = "ToolStripCancel";
            this.ToolStripCancel.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.ToolStripCancel.Size = new System.Drawing.Size(202, 22);
            this.ToolStripCancel.Text = "Отмена";
            this.ToolStripCancel.Click += new System.EventHandler(this.CancelPress);
            // 
            // CoordsLabel
            // 
            this.CoordsLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CoordsLabel.Location = new System.Drawing.Point(1, 375);
            this.CoordsLabel.Name = "CoordsLabel";
            this.CoordsLabel.Size = new System.Drawing.Size(80, 30);
            this.CoordsLabel.TabIndex = 8;
            this.CoordsLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // contur
            // 
            this.contur.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.contur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.contur.Location = new System.Drawing.Point(12, 29);
            this.contur.Name = "contur";
            this.contur.Size = new System.Drawing.Size(30, 30);
            this.contur.TabIndex = 9;
            this.contur.MouseClick += new System.Windows.Forms.MouseEventHandler(this.contur_MouseClick);
            // 
            // filling
            // 
            this.filling.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.filling.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.filling.Location = new System.Drawing.Point(48, 29);
            this.filling.Name = "filling";
            this.filling.Size = new System.Drawing.Size(30, 30);
            this.filling.TabIndex = 10;
            this.filling.MouseClick += new System.Windows.Forms.MouseEventHandler(this.filling_MouseClick);
            // 
            // fillcheck
            // 
            this.fillcheck.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.fillcheck.AutoSize = true;
            this.fillcheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.fillcheck.Location = new System.Drawing.Point(12, 65);
            this.fillcheck.Name = "fillcheck";
            this.fillcheck.Size = new System.Drawing.Size(69, 17);
            this.fillcheck.TabIndex = 0;
            this.fillcheck.Text = "Заливка";
            this.fillcheck.UseVisualStyleBackColor = true;
            this.fillcheck.CheckedChanged += new System.EventHandler(this.fillcheck_CheckedChanged_1);
            // 
            // InsLine
            // 
            this.InsLine.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.InsLine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("InsLine.BackgroundImage")));
            this.InsLine.Location = new System.Drawing.Point(48, 88);
            this.InsLine.Name = "InsLine";
            this.InsLine.Size = new System.Drawing.Size(30, 30);
            this.InsLine.TabIndex = 12;
            this.InsLine.MouseClick += new System.Windows.Forms.MouseEventHandler(this.InsLine_MouseClick);
            // 
            // InsCursor
            // 
            this.InsCursor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.InsCursor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("InsCursor.BackgroundImage")));
            this.InsCursor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InsCursor.Location = new System.Drawing.Point(12, 88);
            this.InsCursor.Name = "InsCursor";
            this.InsCursor.Size = new System.Drawing.Size(30, 30);
            this.InsCursor.TabIndex = 11;
            this.InsCursor.MouseClick += new System.Windows.Forms.MouseEventHandler(this.InsCursor_MouseClick);
            // 
            // InsEllipse
            // 
            this.InsEllipse.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.InsEllipse.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("InsEllipse.BackgroundImage")));
            this.InsEllipse.Location = new System.Drawing.Point(48, 124);
            this.InsEllipse.Name = "InsEllipse";
            this.InsEllipse.Size = new System.Drawing.Size(30, 30);
            this.InsEllipse.TabIndex = 12;
            this.InsEllipse.MouseClick += new System.Windows.Forms.MouseEventHandler(this.InsEllipse_MouseClick);
            // 
            // InsRectangle
            // 
            this.InsRectangle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.InsRectangle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("InsRectangle.BackgroundImage")));
            this.InsRectangle.Location = new System.Drawing.Point(12, 124);
            this.InsRectangle.Name = "InsRectangle";
            this.InsRectangle.Size = new System.Drawing.Size(30, 30);
            this.InsRectangle.TabIndex = 11;
            this.InsRectangle.MouseClick += new System.Windows.Forms.MouseEventHandler(this.InsRectangle_MouseClick);
            // 
            // отменитьВыделениеToolStripMenuItem
            // 
            this.отменитьВыделениеToolStripMenuItem.Name = "отменитьВыделениеToolStripMenuItem";
            this.отменитьВыделениеToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.отменитьВыделениеToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.отменитьВыделениеToolStripMenuItem.Text = "Отменить выделение";
            this.отменитьВыделениеToolStripMenuItem.Click += new System.EventHandler(this.SelectionCancelClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(691, 414);
            this.Controls.Add(this.InsEllipse);
            this.Controls.Add(this.InsRectangle);
            this.Controls.Add(this.InsLine);
            this.Controls.Add(this.InsCursor);
            this.Controls.Add(this.fillcheck);
            this.Controls.Add(this.filling);
            this.Controls.Add(this.contur);
            this.Controls.Add(this.CoordsLabel);
            this.Controls.Add(this.canvas);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Vectred";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.canvas)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox canvas;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label CoordsLabel;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Panel contur;
        private System.Windows.Forms.Panel filling;
        private System.Windows.Forms.CheckBox fillcheck;
        private System.Windows.Forms.Panel InsLine;
        private System.Windows.Forms.Panel InsCursor;
        private System.Windows.Forms.Panel InsEllipse;
        private System.Windows.Forms.Panel InsRectangle;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenFile;
        private System.Windows.Forms.ToolStripMenuItem SaveAs;
        private System.Windows.Forms.ToolStripMenuItem ToolStripNewFile;
        private System.Windows.Forms.ToolStripMenuItem ToolStripCancel;
        private System.Windows.Forms.ToolStripMenuItem отменитьВыделениеToolStripMenuItem;
    }
}

