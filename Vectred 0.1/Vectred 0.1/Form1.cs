﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vectred_0._1
{
    public partial class Form1 : Form
    {
        Point startP;
        Instruments ins;
        Pen pen;
        SolidBrush brush;
        bool drawing = false;
        bool drag = false;
        bool resize = false;
        string s;
        List<VisualObject> visualObjects = new List<VisualObject>();
        VisualObject previewObject;
        VisualObject redactObject;
        Point c1;
        Point c2;
        Point c3;
        Point c4;

        enum Instruments
        {
            Cursor,
            Line,
            Rectangle,
            Ellipse
        }

        public Form1()
        {
            InitializeComponent();
            MinimumSize = Size;
            contur.BackColor = Color.Black;
            filling.BackColor = Color.White;
            pen = new Pen(contur.BackColor);
            brush = new SolidBrush(filling.BackColor);
            KeyPreview = true;
        }

        #region основные обработчики событий

        private void canvas_MouseDown(object sender, MouseEventArgs e)
        {
            if (redactObject != null)
            {
                c1 = redactObject.Coord1;
                c2 = redactObject.Coord2;
                c3 = redactObject.Coord3;
                c4 = redactObject.Coord4;
                if (RegCheck(e) != null)
                {
                    s = RegCheck(e);
                    resize = true; 
                }
                else if (!redactObject.Region.IsVisible(e.Location) && RegCheck(e) == null | ins != 0)
                {
                    SelectionCancel();
                }
                else
                {
                    drag = true;
                }
            }
            startP = e.Location;
            if (ins != 0)
            {
                drawing = true;
            }
            canvas.Refresh();
        }

        private void canvas_MouseMove(object sender, MouseEventArgs e)
        {
            CoordsLabel.Text = e.Location.ToString();
            
            if (drawing)
            {
                switch (ins)
                {
                    case Instruments.Line:
                        previewObject = new VisualLine(pen, startP, e.Location);
                        break;
                    case Instruments.Rectangle:
                        if (fillcheck.Checked)
                            previewObject = new VisualRectangle(pen, brush, startP, e.Location);
                        else
                            previewObject = new VisualRectangle(pen, startP, e.Location);
                        break;
                    case Instruments.Ellipse:
                        if (fillcheck.Checked)
                            previewObject = new VisualEllipse(pen, brush, startP, e.Location);
                        else
                            previewObject = new VisualEllipse(pen, startP, e.Location);
                        break;
                }
                canvas.Refresh();
            }

            #region отображение курсоров

            if (ins == 0)
                canvas.Cursor = Cursors.Arrow;
            else
                canvas.Cursor = Cursors.Cross;
            if (redactObject != null)
                if (redactObject.Region.IsVisible(e.Location))
                    canvas.Cursor = Cursors.SizeAll;

            if (redactObject != null)
            {
                if (RegCheck(e) != null)
                    switch (RegCheck(e))
                    {
                        case "tm":
                            canvas.Cursor = Cursors.SizeNS;
                            break;
                        case "rm":
                            canvas.Cursor = Cursors.SizeWE;
                            break;
                        case "bm":
                            canvas.Cursor = Cursors.SizeNS;
                            break;
                        case "lm":
                            canvas.Cursor = Cursors.SizeWE;
                            break;
                        case "tl":
                            canvas.Cursor = Cursors.SizeNWSE;
                            break;
                        case "tr":
                            canvas.Cursor = Cursors.SizeNESW;
                            break;
                        case "br":
                            canvas.Cursor = Cursors.SizeNWSE;
                            break;
                        case "bl":
                            canvas.Cursor = Cursors.SizeNESW;
                            break;
                    }
            }

            #endregion

            if (resize)
            {
                int dx = startP.X - e.X;
                int dy = startP.Y - e.Y;
                switch (s)
                {
                    case "tm":
                        redactObject.Coord1 = new Point(c1.X, c1.Y - dy);
                        redactObject.Coord2 = new Point(c2.X, c2.Y - dy);
                        canvas.Cursor = Cursors.SizeNS;
                        break;
                    case "rm":
                        redactObject.Coord2 = new Point(c2.X - dx, c2.Y);
                        redactObject.Coord3 = new Point(c3.X - dx, c3.Y);
                        canvas.Cursor = Cursors.SizeWE;
                        break;
                    case "bm":
                        redactObject.Coord3 = new Point(c3.X, c3.Y - dy);
                        redactObject.Coord4 = new Point(c4.X, c4.Y - dy);
                        canvas.Cursor = Cursors.SizeNS;
                        break;
                    case "lm":
                        redactObject.Coord1 = new Point(c1.X - dx, c1.Y);
                        redactObject.Coord4 = new Point(c4.X - dx, c4.Y);
                        canvas.Cursor = Cursors.SizeWE;
                        break;
                    case "tl":
                        redactObject.Coord1 = new Point(c1.X - dx, c1.Y - dy);
                        redactObject.Coord2 = new Point(c2.X, c2.Y - dy);
                        redactObject.Coord4 = new Point(c4.X - dx, c4.Y);
                        canvas.Cursor = Cursors.SizeNWSE;
                        break;
                    case "tr":
                        redactObject.Coord2 = new Point(c2.X - dx, c2.Y - dy);
                        redactObject.Coord1 = new Point(c1.X, c1.Y - dy);
                        redactObject.Coord3 = new Point(c3.X - dx, c3.Y);
                        canvas.Cursor = Cursors.SizeNESW;
                        break;
                    case "br":
                        redactObject.Coord2 = new Point(c2.X - dx, c2.Y);
                        redactObject.Coord3 = new Point(c3.X - dx, c3.Y - dy);
                        redactObject.Coord4 = new Point(c4.X, c4.Y - dy);
                        canvas.Cursor = Cursors.SizeNWSE;
                        break;
                    case "bl":
                        redactObject.Coord1 = new Point(c1.X - dx, c1.Y);
                        redactObject.Coord4 = new Point(c4.X - dx, c4.Y - dy);
                        redactObject.Coord3 = new Point(c3.X, c3.Y - dy);
                        canvas.Cursor = Cursors.SizeNESW;
                        break;
                }
                canvas.Refresh();
            }
            else if (drag)
            {
                int dx = startP.X - e.X;
                int dy = startP.Y - e.Y;
                redactObject.Coord1 = new Point(c1.X - dx, c1.Y - dy);
                redactObject.Coord2 = new Point(c2.X - dx, c2.Y - dy);
                redactObject.Coord3 = new Point(c3.X - dx, c3.Y - dy);
                redactObject.Coord4 = new Point(c4.X - dx, c4.Y - dy);
                canvas.Refresh();
            }
        }

        private void canvas_MouseUp(object sender, MouseEventArgs e)
        {
            if (previewObject != null)
            {
                visualObjects.Add(previewObject);
                previewObject = null;
                drawing = false;
            }

            drag = false;
            resize = false;
        }

        private void canvas_MouseClick(object sender, MouseEventArgs e)
        {
            if (ins == 0)
            {
                for (int i = visualObjects.Count - 1; i >= 0; i--)
                {
                    if (visualObjects[i].HitTest(e.Location) & redactObject == null)
                    {
                        visualObjects[i].Selected = true;
                        redactObject = visualObjects[i];
                        contur.BackColor = visualObjects[i].PenColor;
                        if (visualObjects[i].Filled)
                            filling.BackColor = visualObjects[i].BrushColor;
                        else filling.BackColor = Color.White;
                        fillcheck.Checked = visualObjects[i].Filled;
                        break;
                    }
                }
            }
            canvas.Refresh();
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            foreach (VisualObject vo in visualObjects)
            {
                vo.Draw(e.Graphics);
                vo.DrawOverlay(e.Graphics);
            }
            if (drawing && previewObject != null)
            {
                previewObject.Draw(e.Graphics);
            }
        }

        private void canvas_MouseEnter(object sender, EventArgs e)
        {
            if (ins == 0)
                canvas.Cursor = Cursors.Arrow;
            else
            {
                SelectionCancel();
                canvas.Cursor = Cursors.Cross;
            }
        }

        private void canvas_MouseLeave(object sender, EventArgs e)
        {
            CoordsLabel.Text = string.Empty;
        }

        #endregion

        #region сохранение/открытие файла

        private void SaveAs_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.AddExtension = true;
            dialog.Filter = "Windows Meta File(*.WMF)|*.WMF|Extended Meta File(*.EMF)|*.EMF|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                SelectionCancel();
                Bitmap bmp = new Bitmap(canvas.Width, canvas.Height);
                canvas.DrawToBitmap(bmp, new Rectangle(0, 0, canvas.Width, canvas.Height));
                if (dialog.FilterIndex == 0)
                    bmp.Save(dialog.FileName, ImageFormat.Wmf);
                else if(dialog.FilterIndex == 1)
                    bmp.Save(dialog.FileName, ImageFormat.Emf);
                else
                    bmp.Save(dialog.FileName);
            }
        }

        private void OpenFile_Click(object sender, EventArgs e)
        {
            Bitmap image;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Meta Files(*.WMF;*.EMF)|*.WMF;*.EMF|All files (*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    NewFile();
                    image = new Bitmap(dialog.FileName);
                    canvas.Image = image;
                    canvas.Invalidate();
                }
                catch
                {
                    MessageBox.Show("Невозможно открыть выбранный файл", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        #endregion

        #region выбор цветов, завливки

        private void contur_MouseClick(object sender, MouseEventArgs e)
        {
            colorDialog1.ShowDialog();
            if (redactObject != null)
            {
                redactObject.PenColor = colorDialog1.Color;
            }
            pen.Color = colorDialog1.Color;
            contur.BackColor = colorDialog1.Color;
            canvas.Refresh();
        }

        private void filling_MouseClick(object sender, MouseEventArgs e)
        {
            if (!fillcheck.Checked)
                fillcheck.Checked = true;
            colorDialog1.ShowDialog();
            brush = new SolidBrush(colorDialog1.Color);
            if (redactObject != null)
            {
                redactObject.BrushColor = colorDialog1.Color;
                redactObject.Filled = true;
            }
            filling.BackColor = colorDialog1.Color;
            canvas.Refresh();
        }

        private void fillcheck_CheckedChanged_1(object sender, EventArgs e)
        {
            if (fillcheck.Checked)
            {
                if (redactObject != null)
                {
                    redactObject.Filled = true;
                    redactObject.BrushColor = filling.BackColor;
                }
                brush = new SolidBrush(filling.BackColor);
            }
            else
            {
                if (redactObject != null)
                    redactObject.Filled = false;
                brush.Dispose();
                brush = null;
            }
            canvas.Refresh();
        }

        #endregion

        #region выбор инструмента

        private void InsCursor_MouseClick(object sender, MouseEventArgs e)
        {
            ins = Instruments.Cursor;
            InsCursor.BorderStyle = BorderStyle.FixedSingle;
            InsLine.BorderStyle = BorderStyle.None;
            InsRectangle.BorderStyle = BorderStyle.None;
            InsEllipse.BorderStyle = BorderStyle.None;
        }

        private void InsLine_MouseClick(object sender, MouseEventArgs e)
        {
            ins = Instruments.Line;
            InsCursor.BorderStyle = BorderStyle.None;
            InsLine.BorderStyle = BorderStyle.FixedSingle;
            InsRectangle.BorderStyle = BorderStyle.None;
            InsEllipse.BorderStyle = BorderStyle.None;
        }

        private void InsRectangle_MouseClick(object sender, MouseEventArgs e)
        {
            ins = Instruments.Rectangle;
            InsCursor.BorderStyle = BorderStyle.None;
            InsLine.BorderStyle = BorderStyle.None;
            InsRectangle.BorderStyle = BorderStyle.FixedSingle;
            InsEllipse.BorderStyle = BorderStyle.None;
        }

        private void InsEllipse_MouseClick(object sender, MouseEventArgs e)
        {
            ins = Instruments.Ellipse;
            InsCursor.BorderStyle = BorderStyle.None;
            InsLine.BorderStyle = BorderStyle.None;
            InsRectangle.BorderStyle = BorderStyle.None;
            InsEllipse.BorderStyle = BorderStyle.FixedSingle;
        }

        #endregion

        private void CancelPress(object sender, EventArgs e)
        {
            Cancel();
        }

        private void Cancel()
        {
            if (visualObjects.Count > 0)
            {
                visualObjects.Remove(visualObjects.Last());
                canvas.Refresh();
            }
        }

        private void NewFile()
        {
            visualObjects.Clear();
            canvas.Image = null;
            canvas.Invalidate();
            canvas.Refresh();
        }

        private void TSNewFile(object sender, EventArgs e)
        {
            NewFile();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && redactObject != null)
            {
                visualObjects.Remove(redactObject);
                SelectionCancel();
            }
        }

        private string RegCheck(MouseEventArgs e)
        {
            if (redactObject.TopMiddle.IsVisible(e.Location))
                return "tm";
            else if (redactObject.RightMiddle.IsVisible(e.Location))
                return "rm";
            else if (redactObject.BottomMiddle.IsVisible(e.Location))
                return "bm";
            else if (redactObject.LeftMiddle.IsVisible(e.Location))
                return "lm";
            else if (redactObject.TopLeft.IsVisible(e.Location))
                return "tl";
            else if (redactObject.TopRight.IsVisible(e.Location))
                return "tr";
            else if (redactObject.BottomRight.IsVisible(e.Location))
                return "br";
            else if (redactObject.BottomLeft.IsVisible(e.Location))
                return "bl";
            else return null;
        }

        private void SelectionCancel()
        {
            foreach (VisualObject vo in visualObjects)
            {
                vo.Selected = false;
            }
            redactObject = null;
            canvas.Refresh();
        }

        private void SelectionCancelClick(object sender, EventArgs e)
        {
            SelectionCancel();
        }
    }
}
